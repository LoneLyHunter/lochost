<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

// Панель обычного пользователя
Route::get('/home/ftp/users', 'UserController@ftp_user_view');
Route::get('/home/ftp/filemanager', 'UserController@ftp_filemanager_view');
Route::get('/home/backup', 'UserController@backup_view');
Route::get('/home/db/users', 'UserController@db_users_view');
Route::get('/home/db/database', 'UserController@db_database_view');
Route::get('/home/hosts', 'UserController@view_hosts');
Route::post('/home/hosts/add', 'UserController@add_domain');

Route::get('/home/error', 'UserController@error');

// Панель администратора
Route::get('/admin', 'AdminController@index_view');

