@extends('layouts.app')

@section('content')
    <div class="container" style="padding: 60px 0;">
        <div class="row">
                <form action="{{ url('/home/hosts/add') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="name_domain" placeholder="Домен (без www)" required>
                    </div>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="name_db_domain" placeholder="Название базы данных (необ)">
                    </div>
                    <div class="col-md-12">
                        <button class="btn btn-success" style="margin: 10px 0;">Добавить сайт</button>
                    </div>
                </form>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Домен</th>
                            <th>Путь к папке</th>
                            <th>Удалить</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($domains as $domain)
                        <tr>
                            <td>{{ $domain->name_domain }}</td>
                            <td><a href="">Редактировать в менеджере</a></td>
                            <td>
                                <form action="">
                                    {{ csrf_field() }}
                                    <input type="hidden" value="" name="id">
                                    <button class="btn btn-danger">Удалить</button>
                                </form>
                            </td>
                        </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection