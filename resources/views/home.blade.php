@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">FTP-менеджер</div>

                <div class="panel-body">
                    <a href="{{ url('/home/ftp/users') }}" class="block-panel">
                        <img src="{{ asset('img/icons/users.png') }}" alt="">
                        <span>FTP-Пользователи</span>
                    </a>
                    <a href="" class="block-panel">
                        <img src="{{ asset('img/icons/server.png') }}" alt="">
                        <span>Файловый менеджер</span>
                    </a>
                    <a href="{{ url('/home/hosts') }}" class="block-panel">
                        <img src="{{ asset('img/icons/server.png') }}" alt="">
                        <span>Виртуальные хосты</span>
                    </a>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Базы данных</div>
                <div class="panel-body">
                    <a href="" class="block-panel">
                        <img src="{{ asset('img/icons/server.png') }}" alt="">
                        <span>phpMyAdmin</span>
                    </a>
                    <a href="" class="block-panel">
                        <img src="{{ asset('img/icons/users.png') }}" alt="">
                        <span>Пользователи</span>
                    </a>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Бэкапы</div>
                <div class="panel-body">
                    <a href="" class="block-panel">
                        <img src="{{ asset('img/icons/server.png') }}" alt="">
                        <span>Бэкапы</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-success">
                <div class="panel-heading">Пользователь</div>
                <div class="panel-body">
                    <p style="font-size: 14px; margin: 0;">Пользователь: <span style="color: #3498DB">{{ Auth::user()->name }}</span></p>
                    <p style="font-size: 14px; margin: 0;">Адрес сервера: <span style="color: #3498DB">127.0.0.1</span></p>
                    <p style="font-size: 14px; margin: 0 0 20px 0;">Директория: <span style="color: #3498DB">/var/www/func.com/public_html</span></p>
                    <div style="float: left;">
                    <form id="logout-form" action="{{ route('logout') }}" method="POST">
                        {{ csrf_field() }}
                        <button class="btn btn-warning" style="background: #34495E">Выйти</button>
                    </form>
                    </div>
                    <div style="float: left; margin-left: 15px;">
                    <form id="logout-form" action="{{ route('logout') }}" method="POST">
                        {{ csrf_field() }}
                        <button class="btn btn-warning" style="background: #7F8C8D">Настройки</button>
                    </form>
                    </div> 
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
