@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>Упс... Ошибочка</h1>

                <div class="alert alert-warning">
                    {{ session('error') }}
                </div>
            </div>
        </div>
    </div>
@endsection
