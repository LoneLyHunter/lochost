@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
        	<table class="table table-bordered table-striped">
			    <thead>
			        <tr>
			            <th>Отметить</th>
			            <th>Имя</th>
			            <th>Фамилия</th>
			            <th>E-mail</th>
			        </tr>
			    </thead>
			    <tbody>
			        <tr>
			            <td><input type="checkbox"></td>
			            <td>Иван</td>
			            <td>Чмель</td>
			            <td>ivan@mail.ru</td>
			        </tr>
			        <tr>
			            <td><input type="checkbox"></td>
			            <td>Петр</td>
			            <td>Щербаков</td>
			            <td>petr@mail.ru</td>
			        </tr>
			        <tr>
			            <td><input type="checkbox"></td>
			            <td>Юрий</td>
			            <td>Голов</td>
			            <td>yuri@mail.ru</td>
			        </tr>
			    </tbody>
			</table>
        </div>

    </div>
</div>
@endsection