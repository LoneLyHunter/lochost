<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FtpUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ftp_users', function(Blueprint $table) {
            $table->increments('id_ftp_user');
            $table->string('login_ftp_user');
            $table->string('password_ftp_user');
            $table->string('ip_server_ftp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ftp_users');
    }
}
