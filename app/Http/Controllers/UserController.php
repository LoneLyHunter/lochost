<?php

namespace App\Http\Controllers;

use File;
use Illuminate\Http\Request;
use App\User;
use App\Directory;
use App\ftp_user;
use App\Domain;
use App\db_user;
use DB;
use Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    // FTP 

    public function ftp_user_view()
    {
        $id_user = Auth::user()->id_ftp_user;
        $users = ftp_user::where('id_ftp_user', '=', $id_user);
        return view('ftp.users');
    }

    public function ftp_filemanager_view()
    {
        return view('ftp.filemanager');
    }


    // Database

    public function db_users_view()
    {
        return view('db.users');
    }

    public function db_database_view()
    {
        return view('db.database');
    }


    // Backup

    public function backup_view()
    {
        return vieW('backup.main');
    }

    public function view_hosts()
    {
        $domains = Domain::all();
        return view('hosts.index', ["domains" => $domains]);
    }


    // Domain Adding

    public function add_domain(Request $request)
    {
        // ВНИМАНИЕ !!! ВЫ ДОЛЖНЫ СМЕНИТЬ АБСОЛЮТНЫЕ ПУТИ ПОД ВАШУ СИСТЕМУ:
        $pathToHtdocs = "D:/XAMPP_Second/htdocs";
        $pathToFileHosts = "D:/XAMPP_Second/apache/conf/extra/httpd-vhosts.conf";
        $pathToHosts = "C:/Windows/System32/drivers/etc/hosts";
        // Генерация пароля
        $pass = Str::random(10);

        $messages = [
            'name_domain.required' => "Поле домен обязательно к заполнению !",
            'name_domain.string' => "Поле домен должно быть строкой",
            'name_domain.max' => "Макс.возможное значение для домена - 255 символов"
        ];

        $this->validate($request, [
            'name_domain' => 'max:255|string|required'
        ], $messages);

        $name_domain = $request->get('name_domain');
        $name_db_domain = $request->get('name_db_domain');

        $indexContentFile = "
<DOCTYPE html>
<html>
<head>
    <title>$name_domain</title>
    <meta charset=\"utf-8\">
    <meta http-equiv='X-UA-Compatible' content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
</head>

<h1>$name_domain</h1>

</html>
        ";

        if (File::makeDirectory($pathToHtdocs."/".$name_domain)) {
            // Если файл хостов httpd-vhosts существует
            if (File::get($pathToFileHosts)) {
                // Так нужно расположить переменную для того чтобы в файле все было ОК.
                $host =
                    "
    # $name_domain
    <VirtualHost *:80>
        ServerAdmin webmaster@$name_domain
        DocumentRoot \"$pathToHtdocs/$name_domain\"
        ServerName $name_domain
        ServerAlias www.$name_domain
        ErrorLog \"logs/$name_domain-error.log\"
        CustomLog \"logs/$name_domain-access.log\" common
    </VirtualHost>
                ";
                if (File::prepend($pathToHtdocs."/"."$name_domain/index.html", $indexContentFile)) {
                    // Запись в файл Хоста
                    $writeFile = File::append($pathToFileHosts, $host);
                    if ($writeFile === false) {
                        return redirect("/home/error")->with('error', 'Произошла ошибка, свяжитесь с администратором или проверьте путь к файлу httpd-vhosts.');
                    } else {
                        // Если запись прошла успешно возвращаем пользователя на пред страницу
                        if (File::get($pathToHosts)) {

                            $rowDomain = "\r\n127.0.0.1   $name_domain";

                            File::append($pathToHosts, $rowDomain);
                        } else {
                            return redirect("/home/error")->with('error', 'Ошибка! Разрешите всем группам пользователей доступ к файлу hosts.');
                        }

                        if(Domain::insert(["name_domain" => $name_domain]))
                        {

                            if(empty($name_db_domain))
                            {
                                return back();
                            } else {
                                DB::statement("CREATE DATABASE $name_db_domain CHARACTER SET 'utf8'");
                                db_user::insert(["login_db" => $name_db_domain.".user", "password_db" => $pass, "name_db" => $name_db_domain]);
                                DB::statement("CREATE USER '$name_db_domain.user'@'localhost' IDENTIFIED BY '$pass'");
                                DB::statement("GRANT ALL PRIVILEGES ON $name_db_domain.* TO '$name_db_domain.user'@'localhost';");
                                return back();
                            }
                        } else {
                            return redirect("/home/error")->with('error', 'Ошибка! Ваши данные не занеслись в базу.');
                        }
                    }
                }
            }
        } else {
            return redirect("/home/error")->with('error', 'Ошибка, проверьте указанные пути к файлам и папкам.');
        }
    }

    public function error() {
        return view("error");
    }

}
